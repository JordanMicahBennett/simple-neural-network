Author : Jordan Micah Bennett



????
=======
Synthetic sentience:

i.Encompasses vision detection. See [INSTRUCTIONS-VISUAL.md](https://github.com/JordanMicahBennett/SYNTHETIC-SENTIENCE/blob/master/INSTRUCTIONS-VISUAL.md).

ii.Encompasses xor detection. See [INSTRUCTIONS-XOR.md](https://github.com/JordanMicahBennett/SYNTHETIC-SENTIENCE/blob/master/INSTRUCTIONS-XOR.md).
	
	
	
HIBERNATION
=======
Quintessentially, any deep toggle event [detect, quantize, recall, instruct] 
visually hibernates interface (pause). {...for periods on the order of detection event,
with respect to available cpu cycles}

Visual hibernation 'ceases' the rotating circle at top-left interface.

Rotation re-commences whence visual hibernation terminates. {...indication of instruction cycle completion}
	
