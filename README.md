OUTLINE
============================================
A scratch written, regressive/progressive propagation therein gradient descent aligned artificial neural network framework program of mine, that reasonably models human neuronal phenomena.




DEMONSTRATION
============================================
The source sample includes demonstrations, configured via ( value * value * value ) neuron topology (this is extensible see ...USES); such that such:


i.Perceives handwritten numerals ( 1024x1024x10 neurons ). Conclusively, said neural network aptly perceives priorly unseen input numerals. (including terribly or incompletely written input numerals)

![Alt text](https://github.com/JordanMicahBennett/SYNTHETIC-SENTIENCE/blob/master/source-code/data/images/captures/0.png?raw=true "default page")
============================================


ii.Perceives xor input vectors ( 2x2x1 neurons ).

![Alt text](https://github.com/JordanMicahBennett/SYNTHETIC-SENTIENCE/blob/master/source-code/data/images/captures/1.png?raw=true "default page")
============================================




USES
============================================
This neural network utilizes a simplistic topological mechanism, therein encompassing the construction of sequentially hierarchically horizontal patterns of knowledge genera/perception.

![Alt text](https://github.com/JordanMicahBennett/SYNTHETIC-SENTIENCE/blob/master/source-code/data/images/captures/2.png?raw=true "default page")
============================================

![Alt text](https://github.com/JordanMicahBennett/SYNTHETIC-SENTIENCE/blob/master/source-code/data/images/captures/3.png?raw=true "default page")
============================================



INSTRUCTIONS
============================================
See [INSTRUCTIONS.md](https://github.com/JordanMicahBennett/SYNTHETIC-SENTIENCE/blob/master/INSTRUCTIONS.md).





AUTHOR PORTFOLIO
============================================
http://folioverse.appspot.com/
